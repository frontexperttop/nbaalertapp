import { Component } from '@angular/core';

/**
 * Generated class for the GameTeamItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-team-item',
  templateUrl: 'game-team-item.html'
})
export class GameTeamItemComponent {

  text: string;

  constructor() {
    console.log('Hello GameTeamItemComponent Component');
    this.text = 'Hello World';
  }

}
