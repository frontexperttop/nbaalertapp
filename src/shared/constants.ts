export const CALENDAR = [
	{
		name: "All Weeks",
		value: "object:7"
	},
	{
		name: "Week 1",
		value: "object:8"
	},
	{
		name: "Week 2",
		value: "object:9"
	},
	{
		name: "Week 3",
		value: "object:10"
	},
	{
		name: "Week 4",
		value: "object:11"
	},
	{
		name: "Week 5",
		value: "object:12"
	},
	{
		name: "Week 6",
		value: "object:13"
	},
	{
		name: "Week 7",
		value: "object:14"
	},
	{
		name: "Week 8",
		value: "object:15"
	},
	{
		name: "Week 9",
		value: "object:16"
	},
	{
		name: "Week 10",
		value: "object:17"
	},
	{
		name: "Week 11",
		value: "object:18"
	},
	{
		name: "Week 12",
		value: "object:19"
	},
	{
		name: "Week 13",
		value: "object:20"
	},
	{
		name: "Week 14",
		value: "object:21"
	},
	{
		name: "Week 15",
		value: "object:22"
	},
	{
		name: "Week 15",
		value: "object:22"
	},
	{
		name: "Week 16",
		value: "object:23"
	},
	{
		name: "Week 17",
		value: "object:24"
	},
	{
		name: "Week 18",
		value: "object:25"
	},
	{
		name: "Week 19",
		value: "object:26"
	},
	{
		name: "Week 20",
		value: "object:27"
	},
	{
		name: "Week 21",
		value: "object:28"
	},
	{
		name: "Week 22",
		value: "object:29"
	},
	{
		name: "Week 23",
		value: "object:30"
	},
	{
		name: "Week 24",
		value: "object:31"
	},
	{
		name: "Week 25",
		value: "object:32"
	},
	{
		name: "Week 26",
		value: "object:33"
	},
	{
		name: "All Months",
		value: "object:34"
	},
	{
		name: "January",
		value: "object:35"
	},
	{
		name: "February",
		value: "object:36"
	},
	{
		name: "March",
		value: "object:37"
	},
	{
		name: "April",
		value: "object:38"
	},
	{
		name: "May",
		value: "object:39"
	},
	{
		name: "June",
		value: "object:40"
	},
	{
		name: "September",
		value: "object:41"
	},
	{
		name: "October",
		value: "object:42"
	},
	{
		name: "November",
		value: "object:43"
	},
	{
		name: "December",
		value: "object:44"
	}
];

export const TEAMS = [
	{
		name: "All Teams",
		value: "object:46"
	},
	{
    name: "Atlanta Hawks",
    value: "object:47"
  },
	{
    name: "Boston Celtics",
    value: "object:48"
  },
	{
    name: "Brooklyn Nets",
    value: "object:49"
  },
	{
    name: "Charlotte Hornets",
    value: "object:50"
  },
	{
    name: "Chicago Bulls",
    value: "object:51"
  },	
  {
    name: "Cleveland Cavaliers",
    value: "object:52"
  },
  {
    name: "Dallas Mavericks",
    value: "object:53"
  },
  {
    name: "Denver Nuggets",
    value: "object:54"
  },
  {
    name: "Detroit Pistons",
    value: "object:55"
  },
  {
    name: "Golden State Warriors",
    value: "object:56"
  },
  {
    name: "Houston Rockets",
    value: "object:57"
  },
  {
    name: "Indiana Pacers",
    value: "object:58"
  },
  {
    name: "LA Clippers",
    value: "object:59"
  },
  {
    name: "Los Angeles Lakers",
    value: "object:60"
  },
  {
    name: "Memphis Grizzlies",
    value: "object:61"
  },
  {
    name: "Miami Heat",
    value: "object:62"
  },
  {
    name: "Milwaukee Bucks",
    value: "object:63"
  },
  {
    name: "Minnesota Timberwolves",
    value: "object:64"
  },
  {
    name: "New Orleans Pelicans",
    value: "object:65"
  },
  {
    name: "New York Knicks",
    value: "object:66"
  },
  {
    name: "Oklahoma City Thunder",
    value: "object:67"
  },
  {
    name: "Orlando Magic",
    value: "object:68"
  },
  {
    name: "Philadelphia 76ers",
    value: "object:69"
  },
  {
    name: "Phoenix Suns",
    value: "object:70"
  },
  {
    name: "Portland Trail Blazers",
    value: "object:71"
  },
  {
    name: "Sacramento Kings",
    value: "object:72"
  },
  {
    name: "San Antonio Spurs",
    value: "object:73"
  },
  {
    name: "Toronto Raptors",
    value: "object:74"
  },
  {
    name: "Utah Jazz",
    value: "object:75"
  },
  {
    name: "Washington Wizards",
    value: "object:76"
  }
];

export const FULL_SCHEDULE_WEEK_JSON_URL: string = "https://data.nba.com/data/10s/v2015/json/mobile_teams/nba/2018/league/00_full_schedule_week.json"