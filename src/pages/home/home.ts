import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ScheduleProvider } from '../../providers/schedule/schedule';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	calendar: string = "object:7";
	teams: string = "object:46";
	schedules: any = [];	

  constructor(public navCtrl: NavController, private _scheduleProvider: ScheduleProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this._scheduleProvider.getSchedule()
	    .then((res: any) => {
	    	console.log(res);
	    })
	    .catch(err => {
	    	console.log('GetSchedule error: ', err);
	    })
  }

}
