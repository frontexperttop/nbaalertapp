import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GamesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-games',
  templateUrl: 'games.html',
})
export class GamesPage {

	selecteDay = new Date().toISOString();
	datePickerMin = "2015";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GamesPage');
  }

  changeDate() {

  }

  selectPrevday() {
  	let date = new Date(this.selecteDay);
  	date.setDate(date.getDate() - 1);
  	this.selecteDay = date.toISOString();
  }

  selectNextday() {
  	let date = new Date(this.selecteDay);
  	date.setDate(date.getDate() + 1);
  	this.selecteDay = date.toISOString();
  }

}
