import { NgModule } from '@angular/core';
import { IonicModule } from "ionic-angular";
import { ScheduleItemGroupComponent } from './schedule-item-group/schedule-item-group';
import { GameTeamItemComponent } from './game-team-item/game-team-item';
@NgModule({
	declarations: [ScheduleItemGroupComponent,
    GameTeamItemComponent],
	imports: [IonicModule],
	exports: [ScheduleItemGroupComponent,
    GameTeamItemComponent]
})
export class ComponentsModule {}
