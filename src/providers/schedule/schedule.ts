import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as Constants from '../../shared/constants';

/*
  Generated class for the ScheduleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ScheduleProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ScheduleProvider Provider');
  }

  public getSchedule() {
  	let promise = new Promise((resolve, reject) => {
  		this.http.get(Constants.FULL_SCHEDULE_WEEK_JSON_URL).subscribe(res => {
  			resolve(res);
  		}, err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

}
