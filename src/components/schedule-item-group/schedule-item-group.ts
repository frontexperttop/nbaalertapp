import { Component } from '@angular/core';

/**
 * Generated class for the ScheduleItemGroupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'schedule-item-group',
  templateUrl: 'schedule-item-group.html'
})
export class ScheduleItemGroupComponent {

  text: string;

  constructor() {
    console.log('Hello ScheduleItemGroupComponent Component');
    this.text = 'Hello World';
  }

}
